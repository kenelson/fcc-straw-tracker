# FCC Straw Tracker

This repository is for the development of the straw tracker by the Michigan group and others.
For general information on the FCCSW code see the [tutorial](https://hep-fcc.github.io/fcc-tutorials/master/index.html).

## Setting up

To begin, source the setup script:
```
source setup.sh
```
if it is your first time setting up the code, you can add the option `--build` to compile the C++ code for the straw tracker.

The setup script sources a `key4hep` software stack release (giving us access to the `ddsim` command), sets environment variables, and installs the straw tube C++ code.
The code has been tested on Alma9 operating system (lxplus).

If you modify the C++ code for the straw tracker, then you need to recompile it:
```
cd StrawTubeTracker/build
cmake -D CMAKE_INSTALL_PREFIX=$PWD/../install ..
make install
source ../install/bin/thisStrawTubeTracker.sh
```
After modifications to the code you must again run `make install` inside the build directory.  You may also need to re-run `cmake` if you add new files.

## Checking geometry

After any change to the detector you must run the geometry overlap checker:
```
ddsim --compactFile IDEA_o1_v03/IDEA_o1_v03.xml --runType run --macroFile steering_files/overlap.mac > output/overlap.out
```

If the full detector is used this will take several minutes.

## Running the full simulation

Before producing any output it is recommended to create a folder called "output" and store results there.
This folder will not be tracked by git.  For example: 

```
mkdir -p output/Argon_12micronStraw
```

To simulate a particle gun sample of monochromatic muons in the positive y direction one can run the command:

```
mkdir -p output/Argon_12micronStraw
ddsim --compactFile IDEA_o1_v03/IDEA_o1_v03.xml \
      --outputFile output/Argon_12micronStraw/45GeVmu.root \
      --steeringFile steering_files/single_muon_steer.py \
      --gun.energy "45*GeV"
```

The steering file sets up the particle gun to shoot a single muon in the positive y direction with a $p_{T}$ of 45 GeV.
The energy can easily be modified on the command line as shown.
If a significant number of steering parameters are needing to be configured on the command line, it may be better to write another steering file.
Provided is additionally `random_phi_perp.py`, which randomizes the phi coordinate but remains in the xy plane.
The default steering file uses Argon gas in the straws with 12 micron thick Mylar straw walls.

It is important to note that if you provide a path to an output directory that does not exist the program will segfault!  This is why the `mkdir` command is supplied right next to the `ddsim` command. They should be run together!

The output `.root` file will have a `TTree` `events` which has a set of branches `MyReadout.position.[x,y,z]`.  These are the true positions of the particle in the sensitive volume of the straw tracker.  All Geant4 are by written out due to the configuration in the steering file.  We want to have a single hit per sensitive detector, so there is a simple python script to patch together multiple steps from each sensitive detector.

To save the truth hits to numpy arrays run:

```
python save_hits_numpy.py -i output/VacuumStraw/45GeVmu.root
```

This converts the most important information in the `.root` file into a format ready to be used by the tracking software.
It extracts hits and stores them in FCCHit objects (see `python/structs.py`).
Each hit keeps track of which type of `Collection` it was extracted from in the original `.root` file, which is useful for tracking, as different sub-detectors have different resolutions.
A list of hits is then stored in an `FCCEvent` object.
The `FCCEvent` object also stores information about the truth pT of the primary muon in the MC simulation.
The output list of events are stored in a `.pkl` file with the same basename as the input file.

## Detector visualization

The pre-compiled binary for exporting to .root format is distributed by dd4hep experts as `dd4hep2root`.  Simply run this script on the top-level configuration file you wish to export:

```
./dd4hep2root -c IDEA_o1_v03/IDEA_o1_v03.xml -o StrawTubeTracker.root
```

Then upload the file into [JSROOT](https://root.cern/js/latest/).
You can right click on a single multilayer and for example do "Draw -> all" to see that multilayer.
In order to visualize the stereo angle, it may be useful to do "Draw -> axes".

## Tracking

Currently, the recommended tracking is the python program `run_tracking.py`.  ACTS is WIP.
It takes as input a `.pkl` file from `save_hits_numpy` as shown above.
Other arguments allow the user to configure the tracking algorithm or parameterization of track.
At the moment, the default parameters are recommended.

The output of the tracking is a `.npy` file storing the results of each fit.
The results can be plotted in a gaussian fit using `run_gfit.py`, with the input being the output `.npy` from the tracking step.