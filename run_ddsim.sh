#!/bin/bash
material=$1
energy=$2
config=$3
setup="Chihao_${material}"

source setup.sh
cd $FCC_STRAW_DIR

compactFile="IDEA_${material}_${config}.xml"

if [ ! -f "IDEA_o1_v03/${compactFile}" ]; then
  echo "Error: Compact file IDEA_o1_v03/${compactFile} does not exist."
  exit 1
fi

ddsim --compactFile "IDEA_o1_v03/${compactFile}" \
      --outputFile "${setup}_${config}_${energy}GeVmu.root" \
      --steeringFile steering_files/single_muon_steer.py \
      --gun.energy "${energy}*GeV"

python save_hits_numpy.py "$config" "${setup}_${config}_${energy}GeVmu.root"
