#!/bin/bash
source /cvmfs/sw.hsf.org/key4hep/setup.sh -r 2024-04-12
cd /atlas/data18a/lyriche/fcc-straw-tracker
source StrawTubeTracker/install/bin/thisStrawTubeTracker.sh

material=$1
config=$2
energy=$3
resolution_straw=$4

python run_gfit.py $material $config $energy $resolution_straw
