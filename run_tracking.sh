#!/bin/bash

source /cvmfs/sw.hsf.org/key4hep/setup.sh -r 2024-04-12
cd /atlas/data18a/lyriche/fcc-straw-tracker
source StrawTubeTracker/install/bin/thisStrawTubeTracker.sh

material=$1
config=$2
resolution_straw=$3
energy=$4

python run_tracking.py $material $config $energy $resolution_straw
