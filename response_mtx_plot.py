
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.colors import LinearSegmentedColormap
from scipy import stats
from scipy.optimize import minimize, curve_fit, root_scalar
from scipy.optimize import NonlinearConstraint
import scipy
from scipy.stats import gamma, chi2, norm
import pickle
import os  ## This module is for "operating system" interfaces
import sys

path_workdir = '/atlas/data18a/joarias/fcc-straw-tracker/'
path_fits_figs = path_workdir + 'gfits_imgs_txt_files/'
root_files_path = path_workdir + 'root_files/'
npy_files_path = path_workdir + 'npy_files/'
pT_fit_txt_files_path = path_workdir + 'pT_fit_txt_files/'

plt.style.use('mplstyle.txt')
plt.rcParams["mathtext.fontset"] = "cm"

## read sys
if len(sys.argv) != 5:
    print("Usage: python simulation_v03.py <integer>")
    sys.exit(1)

material = sys.argv[1]
config = sys.argv[2]
resolution_straw = int(sys.argv[3])

energies = range(10, 105, 5)

all_pT_truth = []
all_pT_reco = []

## Loop over pT (energy) values to load files all at once
for energy in energies:
    if not (energy in [60, 70] and resolution_straw == 100):
        setup=f"Chihao_{material}_{config}_{energy}_{resolution_straw}"
        file_name = f"pT_reco_truth_{setup}_{energy}_{resolution_straw}.txt"
        file_path = os.path.join(path_fits_figs, file_name)

        if os.path.isfile(file_path):
            ## Load the data
            data = np.loadtxt(file_path, skiprows=1)  ## Skip the header row
            all_pT_truth.append(data[:, 0])  
            all_pT_reco.append(data[:, 1])   
            print(f"Loaded: {file_name}")
        else:
            print(f"File not found: {file_name}")

## Convert lists to arrays for easier manipulation
all_pT_truth = np.concatenate(all_pT_truth)
all_pT_reco = np.concatenate(all_pT_reco)
pT_diff = all_pT_reco - all_pT_truth
print(np.shape(all_pT_reco), np.shape(pT_diff) )

#### Plot a Pseudo-Respose Matrix: pT_truth vs. pT_reco
plt.figure(1)

binsize = 1.0
pT_bins = np.arange(0, 110 + binsize, binsize)
line_arr = np.linspace(0.0, 111, 111)

H, pT_truth_yedges, pT_reco_xedges = np.histogram2d(all_pT_truth, all_pT_reco, bins=[pT_bins, pT_bins])

cmap_plt = 'gist_heat'
cmap_reverse = True
if cmap_reverse:
    cmap = plt.get_cmap(cmap_plt + '_r')
    line_color = 'black'
else:
    cmap = plt.get_cmap(cmap_plt)
    line_color = 'white'

color_array = np.linspace(0.0, 0.8, 256)
new_cmap = LinearSegmentedColormap.from_list('truncated_hot', cmap(color_array ))

plt.plot(line_arr, line_arr, color=line_color, linewidth=0.8, alpha =0.4)
mesh = plt.pcolormesh(pT_reco_xedges, pT_truth_yedges, H, cmap=new_cmap) 

if config == "vs":
    plt.annotate(r'$\rm{pixel}+\rm{straw}$', xy=(0.04, 0.92), 
                 xycoords='axes fraction', fontsize=15, ha='left', fontfamily ='serif', 
                 bbox=dict(boxstyle="round,pad=0.3", edgecolor='black', facecolor='white'))
else:
    plt.annotate( r'$\rm{pixel}+\rm{straw}+\rm{wrapper}$', xy=(0.04, 0.92), 
                 xycoords='axes fraction', fontsize=15, ha='left', fontfamily ='serif', 
                bbox=dict(boxstyle="round,pad=0.3", edgecolor='black', facecolor='white'))

plt.annotate(r'%s $\mu \rm{m}$ $\rm{Straw}$ $\rm{Resolution}$' %(resolution_straw), xy=(0.04, 0.82), 
             xycoords='axes fraction', fontsize=15, ha='left', fontfamily ='serif', 
             bbox=dict(boxstyle="round,pad=0.3", edgecolor='black', facecolor='white'))

cbar = plt.colorbar(mesh)
binsize_events = (np.max(pT_bins)-np.min(pT_bins))/len(pT_bins)
cbar.set_label(r'$\rm{Events} \; / \; (%.1f \, \rm{GeV})$' %(binsize_events), fontsize=14)
cbar.ax.tick_params(labelsize=12)

plt.grid(alpha=0.3)
plt.ylabel(r'$p_T^{\rm{truth}}$', fontsize=18)
plt.xlabel(r'$p_T^{\rm{reco}}$', fontsize=18)
plt.ylim(0.0, 110)
plt.xlim(0.0, 110)
plt.tick_params(axis='both', which='major', labelsize=12)
plt.title(r'$p_T^{\rm{truth}}$ $\rm{vs.}$ $p_T^{\rm{reco}}$ $\rm{for}$ $\rm{%s}$ $(\rm{%s})$'%(config, material))
plt.savefig(f'Response_Mtx_{material}_{config}_{resolution_straw}.pdf')
plt.clf()

#### Plot a difference between pT_reco - pT_truth as a function of pT_reco...
plt.figure(2)

plt.scatter(all_pT_truth, pT_diff, color='black', s=2.8, alpha =0.4)

if config == "vs":
    plt.annotate(r'$\rm{pixel}+\rm{straw}$', xy=(0.95, 0.15), 
                 xycoords='axes fraction', fontsize=15, ha='right', fontfamily ='serif', 
                 bbox=dict(boxstyle="round,pad=0.3", edgecolor='black', facecolor='white'))
else:
    plt.annotate( r'$\rm{pixel}+\rm{straw}+\rm{wrapper}$', xy=(0.95, 0.15), 
                 xycoords='axes fraction', fontsize=15, ha='right', fontfamily ='serif', 
                bbox=dict(boxstyle="round,pad=0.3", edgecolor='black', facecolor='white'))

plt.annotate(r'%s $\mu \rm{m}$ $\rm{Straw}$ $\rm{Resolution}$' %(resolution_straw), xy=(0.95, 0.05), 
             xycoords='axes fraction', fontsize=15, ha='right', fontfamily ='serif', 
             bbox=dict(boxstyle="round,pad=0.3", edgecolor='black', facecolor='white'))

plt.grid(alpha=0.3)
plt.ylabel(r'$p_T^{\rm{reco}}-p_T^{\rm{truth}}$', fontsize=18)
plt.xlabel(r'$p_T^{\rm{reco}}$', fontsize=18)
plt.ylim(-110.0, 110)
plt.xlim(0.0, 110)
plt.tick_params(axis='both', which='major', labelsize=12)
title_str = (r'$\Delta p_T^{\rm{reco/truth}}$ $\rm{vs.}$ $p_T^{\rm{reco}}$ $\rm{for}$ $\rm{%s}$ $(\rm{%s})$'%(config, material))
plt.title(title_str)
plt.savefig(f'Difference_pT_{material}_{config}_{resolution_straw}.pdf')
plt.clf()

