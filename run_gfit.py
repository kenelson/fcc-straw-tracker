
"""
python gfit.py [material] [vs or vsw] [energy] [resolution_straw]
it will do gaussian fit for that and generate files like Gaussian_Chihao_Ar_vs_10_100.txt that stores pT, mu, sigma, mu_error, sigma_error
Also it will generate gaussian fit plot like Gaussian_Chihao_Ar_vs_10_100.png
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from scipy.optimize import minimize, curve_fit, root_scalar
from scipy.optimize import NonlinearConstraint
import scipy
from scipy.stats import gamma, chi2, norm
import pickle
import os  ## This module is for "operating system" interfaces
import sys
import argparse

parser = argparse.ArgumentParser(description='Run tracking on an input file created by save_hits_numpy.py')
parser.add_argument('-i', '--input-file',
                    help='Input .npy file name (output of run_tracking.py)',
                    required=True)

args = parser.parse_args()

#plt.style.use('mplstyle.txt')
plt.rcParams["mathtext.fontset"] = "cm"

## cutoff for gaussian fit
def cutoff(pT):
    #return 0.01*np.log10(pT) + 0.01
    return 0.00015

## Gaussian function for fit
def gaussian_model(x, mu, sigma, amplitude):
    return amplitude * (1 / np.sqrt(2 * np.pi*sigma*sigma)) * np.exp(-(x - mu)*(x - mu) / 2 / sigma / sigma)


fit_results = np.load(args.input_file)
pT_resolution = 1.0/fit_results['pT_reco'] - 1.0/fit_results['pT_truth']
#pT_resolution= pT_resolution[np.where((fit_results['NVTXHits']==4) &
#                                      (fit_results['NWrapperHits']==1))]

## Plotting pT_resolution
plt.figure()
bins = 50
sup = np.max(cutoff(fit_results['pT_truth']))
inf = -1*sup
hist, bin_edges = np.histogram(pT_resolution, bins=bins, range=(inf, sup), density=False)
bin_center = (bin_edges[1:] + bin_edges[:-1]) / 2

lnspc = np.linspace(inf, sup, 1000)
valid_indices = (bin_center >= inf) & (bin_center <= sup)

## just to make sure NaN and inf are not in the array
pT_resolution = np.clip(pT_resolution, inf, sup)
pT_resolution_clean = pT_resolution[np.isfinite(pT_resolution)]

## Calculate Poisson errors, replacing zero errors with a small value, 
## or previous non-zero error value in the array...
errors = np.where(hist[valid_indices]==0, 1, np.sqrt(hist[valid_indices]))

print('Mean:', np.mean(pT_resolution_clean), 'Median:', np.median(pT_resolution_clean))
print('Max hist:', np.max(hist), 'E/bins:', 1000 * 2 * sup / bins )

#### CURVE FITTING ###
## Initial guesses
p0 =[-0.002, 0.0010, np.max(hist)*0.001]
lower_bounds = [-0.01/2, 0.0, 0.0]  
upper_bounds = [ 0.01/2, 0.01, np.inf]
print('Init p0: Mean/Median =', p0[0], 'sigma =', p0[1], 'A =', p0[2])

popt, pcov = curve_fit(gaussian_model, bin_center[valid_indices], hist[valid_indices], p0=p0, 
                       sigma = (errors), bounds=(lower_bounds, upper_bounds)
                       )
## Other parms for curve_fit; not much of a difference.
##  maxfev=10000 #N_iter, ftol=1e-10, #residuals_prec xtol=1e-10, #parameters_prec, gtol=1e-10 #gradients_prec

mu, sigma, amplitude = popt
mu_error, sigma_error, amplitude_error = np.sqrt(np.diag(pcov))
gaus_fit = gaussian_model(lnspc, mu, sigma, amplitude)
print(f"mu={mu}, sigma={sigma}, A={amplitude}" )

alpha_hist = 0.6 #0.8

plt.axvline(0.0,  color='blue', linestyle='dotted', alpha = 0.4, lw = 1.3)
plt.hist(pT_resolution, bins, range=(inf, sup), density=False, color='skyblue', alpha=alpha_hist)
plt.plot(lnspc, gaus_fit, 'b--', linewidth=2.3)

errors = np.sqrt(hist[valid_indices])
binsize = (np.max(bin_edges)- np.min(bin_edges)) / bins 
plt.errorbar(bin_center[valid_indices], hist[valid_indices], yerr=errors, 
             markersize=7.0, capsize=5, elinewidth=1.0, zorder=7, alpha=0.7,
             fmt='o', mfc='darkblue', mec='darkblue', ecolor='darkblue' 
             )

'''
plt.annotate(
    r'%s $\rm{GeV}$' %(energy) , 
    xy=(0.95, 0.75), 
    xycoords='axes fraction', 
    fontsize=15, 
    ha='right', 
    fontfamily ='serif', 
    bbox=dict(boxstyle="round,pad=0.3", edgecolor='black', facecolor='white')
)

plt.annotate(
    r'%s $\mu \rm{m}$ $\rm{Straw}$ $\rm{Resolution}$' %(resolution_straw), 
    xy=(0.04, 0.89), 
    xycoords='axes fraction', 
    fontsize=15, 
    ha='left', 
    fontfamily ='serif', 
    bbox=dict(boxstyle="round,pad=0.3", edgecolor='black', facecolor='white')
)
'''
plt.annotate(
    r'$\mu = (%.4f \pm %.4f)$ $\times$ $10^{-3}$' %(mu*1e3, mu_error*1e3), 
    xy=(0.04, 0.79), 
    xycoords='axes fraction', 
    fontsize=13, 
    ha='left', 
    fontfamily ='serif', 
    bbox=dict(boxstyle="round,pad=0.3", edgecolor='white', facecolor='white')
)

plt.annotate(
    r'$\sigma = (%.4f \pm %.4f)$ $\times$ $10^{-3}$' %(sigma*1e3, sigma_error*1e3), 
    xy=(0.04, 0.69), 
    xycoords='axes fraction', 
    fontsize=13, 
    ha='left', 
    fontfamily ='serif', 
    bbox=dict(boxstyle="round,pad=0.3", edgecolor='white', facecolor='white')
)


plt.xlabel(r'$1/p_T^{\rm{reco}} - 1/p_T^{\rm{truth}}$ [1/GeV]', fontsize=18)
plt.ylabel(r'$\rm{Events}$' %(binsize), fontsize=18)
plt.tick_params(axis='both', which='major', labelsize=12)
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
plt.xlim(inf, sup)
plt.ylim(0,2*max(hist))
plt.subplots_adjust(left=0.15, right=0.9, top=0.95, bottom=0.2)

## Replace 'var' with appropriate label
#plt.title(r'$\rm{Gaussian}$ $\rm{Fit}$ $\rm{for}$ $\rm{%s}$ ($\rm{%s}$)'%(config, material))
plt.savefig(args.input_file.replace('_fitresults.npy', '_gfit.pdf'))
plt.clf()
'''
## Define the result list
result = [mu, np.abs(sigma), mu_error, np.abs(sigma_error)]

## Write result to a text file
with open(path_fits_figs + f'Gaussian_{setup}.txt', 'w') as file:
    file.write(f"pT: {energy}\n")
    file.write(f"mu: {mu}\n")
    file.write(f"sigma: {np.abs(sigma)}\n")
    file.write(f"mu_error: {mu_error}\n")
    file.write(f"sigma_error: {np.abs(sigma_error)}\n")

print(f"Fit results saved")
'''
print('outliers:', np.sum(pT_resolution == inf) + np.sum(pT_resolution == sup))
