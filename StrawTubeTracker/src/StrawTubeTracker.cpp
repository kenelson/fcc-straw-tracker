#include "DD4hep/DetFactoryHelper.h"
#include "DDRec/DetectorData.h"
#include "XML/Layering.h"
#include "XML/Utilities.h"
static dd4hep::Ref_t create_detector(dd4hep::Detector &theDetector,
                                     xml_h entities,
                                     dd4hep::SensitiveDetector sens) {

  // XML Detector Element (confusingly also XML::DetElement)
  xml_det_t x_det = entities;
  // DetElement of our detector instance, attach additional information,
  // sub-elements... uses name of detector and ID number as defined in the XML
  // detector tag
  std::string detName = x_det.nameStr();
  sens.setType("tracker");
  dd4hep::DetElement sdet(detName, x_det.id());

  // get the dimensions tag
  xml_dim_t dim = x_det.dimensions();
  // read its attributes
  double rmin = dim.rmin();
  double rmax = dim.rmax();
  double zmax = dim.zmax();
  // Make a Cylinder
  dd4hep::Tube envelope(rmin, rmax, zmax);
  dd4hep::Material gas = theDetector.material("He");
  dd4hep::Volume envelopeVol(detName + "_envelope", envelope, gas);
  dd4hep::PlacedVolume physvol =
    theDetector.pickMotherVolume(sdet).placeVolume(envelopeVol);
  // add system ID and identify as barrel (as opposed to endcap +/-1)
  physvol.addPhysVolID("system", sdet.id()).addPhysVolID(_U(side), 0);
  sdet.setPlacement(physvol);

  dd4hep::PlacedVolume pv;

  // Interpretation of multi-layers: 
  double MLInnerRadius = rmin; // running inner radius
  dd4hep::Layering layering(x_det); // convenience class
  int MLNum = 0, tubeNum = 0;
  double tube_gap = dd4hep::_toDouble("ML1_gap");
  int layer_num = dd4hep::_toInt("LayerNum"); // Numbers of layer per multi-layer

  std::vector<int> sector_num;
  sector_num.emplace_back(1);
  sector_num.emplace_back(dd4hep::_toInt("ML1_SectorNum"));
  sector_num.emplace_back(dd4hep::_toInt("ML2_SectorNum"));
  sector_num.emplace_back(dd4hep::_toInt("ML3_SectorNum"));

  double tubeThickness = 0.0;
  int tubeType = 0;
  for (xml_coll_t c(x_det, _U(layer)); c; ++c, ++MLNum) {
    xml_comp_t x_layer = c;
    // const dd4hep::Layer *lay = layering.layer(MLNum); // Get the layer from the layering engine.
    
    if ( tubeThickness < 2*(layering.singleLayerThickness(x_layer)) )  tubeType += 1; // Tube size changes
    tubeThickness = 2*(layering.singleLayerThickness(x_layer));
    
    // loop over the number of repetitions
    double layerRadius = MLInnerRadius + tubeThickness + tube_gap;
    double MLThickness = x_layer.thickness();
    double delta_phi = asin((0.1 + tubeThickness)*0.5/layerRadius);
    // /dd4hep::pi*180;

    // make a volume for the multi-layer
    std::string MLName = detName + dd4hep::_toString(MLNum, "_multilayer%d");
    dd4hep::Tube MLTube(MLInnerRadius,
                          MLInnerRadius + MLThickness, zmax);
    dd4hep::Volume MLVol(MLName, MLTube, gas);
    // dd4hep::DetElement MLElement(sdet, MLName, MLNum);
    dd4hep::PlacedVolume layerVolPlaced = envelopeVol.placeVolume(MLVol);
    layerVolPlaced.addPhysVolID("multiLayer", MLNum);
    // MLElement.setPlacement(layerVolPlaced);

    for (int j = 0; j < layer_num; ++j, ++tubeNum)
    {
      for (int l = 0; l < sector_num[tubeType]; ++l, ++tubeNum)
      {
        for (int i = 0, repeat = x_layer.repeat(); i < repeat; ++i, ++tubeNum) {
          // tube volume
          std::string tubeName = MLName + dd4hep::_toString(l, "sector%d") + dd4hep::_toString(j, "layer%d") + dd4hep::_toString(i, "tube%d");

          dd4hep::Tube singleTube(0, tubeThickness/2, zmax);
          dd4hep::Volume singleVol(tubeName, singleTube, gas);
          dd4hep::DetElement tubeElement(sdet, tubeName, tubeNum);
          double phi = l*2*dd4hep::pi/sector_num[tubeType] + (j+2*i)*delta_phi;
          // dd4hep::Position tubePosition = (layerRadius*sin(phi),layerRadius*cos(phi),0);
          // dd4hep::PlacedVolume tubePlacedVolume = MLVol.placeVolume(singleVol,tubePosition);
          dd4hep::PlacedVolume tubePlacedVolume = MLVol.placeVolume(singleVol,dd4hep::Position(layerRadius*sin(phi),layerRadius*cos(phi),0));
          tubePlacedVolume.addPhysVolID("sector",l).addPhysVolID("layer",j).addPhysVolID("tube",i);
          tubeElement.setPlacement(tubePlacedVolume);

          //  single tube construction
          double tubeInnerRadius = 0.0;
          int sliceNum = 0;
          for (xml_coll_t slice(x_layer, _U(slice)); slice; ++slice, ++sliceNum) {
            xml_comp_t x_slice = slice;
            double sliceThickness = x_slice.thickness();
            dd4hep::Material sliceMat = theDetector.material(x_slice.materialStr());
            std::string sliceName = MLName + dd4hep::_toString(j, "layer%d") + dd4hep::_toString(i, "tube%d") + x_slice.materialStr() + dd4hep::_toString(sliceNum);
            dd4hep::Tube sliceTube(tubeInnerRadius,
                                  tubeInnerRadius + sliceThickness, zmax);
            dd4hep::Volume sliceVol(sliceName, sliceTube, sliceMat);
            if (x_slice.isSensitive()) {
              sliceVol.setSensitiveDetector(sens);
            }
            // place the slice in the layer
            pv = singleVol.placeVolume(sliceVol);
            // pv.addPhysVolID("submodule",sliceNum + 1);
            tubeInnerRadius += sliceThickness;
          } // slices
        }   // repetitions
      }
      layerRadius += (tubeThickness + tube_gap)*0.866; // equal tube gap
    }
    MLInnerRadius += MLThickness;
  }     // layers

  return sdet;
}
DECLARE_DETELEMENT(StrawTubeTracker, create_detector)
