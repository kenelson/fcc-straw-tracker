import numpy as np

'''
Define some commonly needed geometric relations 
in one place
'''





'''
(x0, y0) is the numpy array of points to which we
want to calculate the distance to the line defined
by the two points (x1, y1) and (x2, y2)
'''
def point_line_distance(x0, y0, x1, y1, x2, y2):
    return np.abs((y2-y1)*x0 - (x2-x1)*y0 + x2*y1 - y2*x1)/np.sqrt((y2-y1)*(y2-y1) + (x2-x1)*(x2-x1))


'''
Compute pT from bending radius [m] and field [T]
return in units of GeV
'''
def bending_radius_to_pT(r, B):
    return 0.3*r*B

'''
Compute bending radius from pT [GeV] and field [T]
return in units of meters
'''
def bending_radius(pT, B):
    return pT/(0.3*B)


'''
Convert from cartesian to polar coordinates in 2D
'''
def cartesian_to_polar(x, y):
    r_data   = np.sqrt(x**2 + y**2)
    phi_data = np.arctan2(y, x)
    return r_data, phi_data

'''
Convert from polar to cartesian coordinates in 2D
'''
def polar_to_cartesian(r, phi):
    x_data = r*np.cos(phi)
    y_data = r*np.sin(phi)
    return x_data, y_data
