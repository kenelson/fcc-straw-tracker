# -*- coding: utf-8 -*-

# import
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from scipy.optimize import minimize
from scipy.optimize import root_scalar
from scipy.optimize import NonlinearConstraint
import scipy
from scipy.stats import gamma
from scipy.stats import chi2
from scipy.stats import norm
import pickle
import os
import sys
import argparse
from fcc.structs import FCCHit, FCCEvent
import fcc.optimizer as fccopt
from fcc.geometry import cartesian_to_polar, polar_to_cartesian

# parse command line arguments
parser = argparse.ArgumentParser(description='Run tracking on an input file created by save_hits_numpy.py')
parser.add_argument('-i', '--input-file',
                    help='Input pkl file name (output of save_hits_numpy.py)',
                    required=True)
parser.add_argument('-r', '--straw-resolution',
                    help='Resolution of straw detector in microns.  Default=120',
                    type=int,
                    default=120)
parser.add_argument('-p', '--parameterization',
                    help='How to parameterize the track.  See python/fcc/optimizer for options.',
                    type=str,
                    default='PtXYTrackParameterization')
parser.add_argument('-o', '--optimizer',
                    help='How to optimize the tracks.  See python/fcc/optimizer for options.',
                    type=str,
#                    default='LinearLeastSqOptimizer')
                    default='SimplexOptimizer')
args = parser.parse_args()

# read the data
with open(args.input_file, 'rb') as inputfile:
    fccevents = pickle.load(inputfile)

# random number generator
rng = np.random.default_rng()

# smear or no smear
smear = 1

# magnetic field in Tesla
magnetic_field = 2

#
# find all hit positons on all layers (r, phi) and its position resolution
# smear it according to the resolution
#
def hit_position(x_data, y_data, resolution):
    r, phi = cartesian_to_polar(x_data,y_data)
    delta_phi = resolution/r*1e-3*np.random.normal(size=resolution.shape)
    x_smeared, y_smeared = polar_to_cartesian(r, phi + delta_phi)
    hits = np.stack((r*0.001, phi + delta_phi, resolution, x_smeared*0.001, y_smeared*0.001), axis=1)
    hits = np.array(list(map(tuple, hits)), dtype=fccopt.hits_dtype)
    return hits

np.random.seed(1)
pT_resolution=[]

for i in range(len(fccevents)):

    x_data     = fccevents[i].xpos()
    y_data     = fccevents[i].ypos()
    
    def hit_res(hit: FCCHit):
        c = hit.collection
        if 'VTX' in c:
            return 5
        if 'MyWrapper' in c:
            return 50/np.sqrt(12)
        if 'MyReadout' in c:
            return args.straw_resolution
        
    resolution = np.array([hit_res(h) for h in fccevents[i].hits])

    truth=fccevents[i].truth_pt
    hits=hit_position(x_data,y_data, resolution)
    if (len(hits) == 0):
        continue

    param = getattr(fccopt, args.parameterization)(magnetic_field=magnetic_field)
    opt   = getattr(fccopt, args.optimizer)(param)
    measured = opt.optimize(hits)

    print(measured.x)
    resolution = 1.0/measured.x[0] - 1.0/truth
    print(fccevents[i].NVTXHits(),
          fccevents[i].NStrawHits(),
          fccevents[i].NWrapperHits())
    
    if not measured.success:
        print("check the trajectory, the plot may be shrimp like")
        continue
    
    pT_resolution.append((measured.x[0],
                          truth,
                          fccevents[i].NVTXHits(),
                          fccevents[i].NStrawHits(),
                          fccevents[i].NWrapperHits()))

pT_resolution = np.array(pT_resolution, dtype=[('pT_reco', 'float64'),
                                               ('pT_truth', 'float64'),
                                               ('NVTXHits', 'int32'),
                                               ('NStrawHits', 'int32'),
                                               ('NWrapperHits', 'int32')])
np.save(args.input_file.replace('.pkl', '_fitresults.npy'), pT_resolution)
