
source /cvmfs/sw.hsf.org/key4hep/setup.sh -r 2024-04-12
export FCC_STRAW_DIR=$PWD
export PYTHONPATH=$PYTHONPATH:${FCC_STRAW_DIR}/python

if test "$1" == "--build"; then
    cd $FCC_STRAW_DIR/StrawTubeTracker
    mkdir build install
    cd build
    cmake -D CMAKE_INSTALL_PREFIX=$PWD/../install ..
    make install
fi
source $FCC_STRAW_DIR/StrawTubeTracker/install/bin/thisStrawTubeTracker.sh
cd $FCC_STRAW_DIR
