import ROOT
import numpy as np
import sys
import argparse
import pickle
from fcc.structs import FCCHit, FCCEvent

# set up argparser for command line options
parser = argparse.ArgumentParser(description='Save hits from .root to .npy format for tracking, and slim hits, removing those hits outside the xy plane')
parser.add_argument('-i', '--input-file',
                    help='Input file name (output of ddsim command)',
                    required=True)
args = parser.parse_args()

# open root input file
file= ROOT.TFile(args.input_file)
events = file.Get("events")

# initialize some counters to check for
# multiple hits on same cellID
repeat = 0
total = 0

def store_hit(hit, hit_dict, cell_id_count, collection_name):
    if hit.cellID in hit_dict.keys():
        hit_dict[hit.cellID] = FCCHit(collection_name,
                                      hit.position.x, hit.position.y, hit.position.z)
        cell_id_count[hit.cellID] += 1
    else:
        hit_dict[hit.cellID] = FCCHit(collection_name,
                                      hit.position.x, hit.position.y, hit.position.z)
        cell_id_count[hit.cellID] = 0

def parse_collection(event,
                     collection_name,
                     hit_dict,
                     cell_id_count):
    if hasattr(event, collection_name):
        for hit in getattr(event, collection_name):
            # TODO:
            # hardcoded for perpendicular hits, need to remove eventually
            if abs(hit.position.z) < 10:
                store_hit(hit,hit_dict,cell_id_count, collection_name)

# list of possible collections
# to save hits from
collections = ['VTXDCollection',
               'VTXIBCollection',
               'VTXOBCollection',
               'MyReadout',
               'CDCHHits',
               'MyWrapper'
               ]

# output FCCEvents list
output_events = []

for event in events:
    cell_id_count = {}
    hit_dict = {}
    pT = 0

    # get the pT from the event
    if not hasattr(event, 'MCParticles'):
        print('ERROR: MCParticles is required branch in TTree event')
        print('       Program exiting...')
        exit(1)
    else:
        for mcp_index in range(len(event.MCParticles)):
            mcp = event.MCParticles[mcp_index]
            if mcp.generatorStatus == 1:
                pT = np.sqrt(mcp.momentum.x*mcp.momentum.x + mcp.momentum.y*mcp.momentum.y)

    if pT ==0:
        print('WARNING: initial state MC particle not found, skipping event.')
        continue
                
    # parse all collections
    for collection in collections:        
        parse_collection(event, collection, hit_dict, cell_id_count)

    # create and save the FCCEvent object
    output_events.append(FCCEvent([hit for _, hit in hit_dict.items()], pT))
        
    # check for repeat hits in same cell ID
    for count in cell_id_count:
        if cell_id_count[count] != 0:
            repeat += cell_id_count[count]
            total += (cell_id_count[count]+1)
        else:
            total += (cell_id_count[count]+1)

print(f"{repeat} out of {total} is repeated, ratio = {repeat/total*100}%")

with open(args.input_file.replace('.root', '.pkl'), 'wb') as outfile:
    pickle.dump(output_events, outfile)

