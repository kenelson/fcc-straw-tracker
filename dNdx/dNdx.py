import numpy as np
from scipy.stats import moyal
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator

def csi(x: float,
        beta: float,
        Z: int = 2,
        A: float = 4,
        z: int = 1):
    # K is a constant (0.307075 MeV mol^{-1} cm^{2})
    # Z is atomic number of absorber
    # A is atomic mass of absorber
    # z is charge number of incident particle
    # x is the detector thickness in g cm^-2 (mass per unit area)
    # beta is relativistic boost
    K = 0.307075
    return K/2*Z/A*z*z*x/beta/beta

def gammabeta(mass: float,
              mom:  float):
    # return the relativistic gamma factor from mass and momentum
    # requires same units for mass and momentum (c=1)
    
    # beta/sqrt(1-beta^2)=mom/mass
    # beta^2=(1-beta^2)*(mom/mass)^2
    # beta^2(1+(mom/mass)^2)=(mom/mass)^2
    mm    = mom/mass
    beta  = np.sqrt(mm*mm/(1+mm*mm))
    gamma = np.sqrt(1+mm*mm)
    return gamma, beta

def delta(momentum:     float, 
          mass:         float, # incident particle
          materialname: str):

    # the computations require fits to data.
    # Use existing references: https://www.sciencedirect.com/science/article/pii/0092640X84900020?via%3Dihub
    # R. M. Sternheimer, M. J. Berger and S. M. Seltzer, Atom. Data Nucl. Data Tabl. 30, 261
    # (1984); Minor errors are corrected in Ref. 5. Chemical composition for the tabulated materials
    # is given in Ref. 10.

    if materialname == 'Helium':
        C = 11.1393
        a = 0.13443
        m = 5.8347
        x0 = 2.2017
        x1 = 3.6122
        delta0 = 0.0
    else:
        raise Exception(f"Material {materialname} not found")

    x = np.log10(momentum/mass)

    if x < x0:
        return delta0*np.power(10, 2*(x-x0))
    elif x < x1:
        return 2*np.log(10)*x-C+a*np.power(x1-x, m)
    else:
        return 2*np.log(10)*x-C

def landau_params(dx: float,   # thickness of detector in cm
                  mom: float,  # momentum in MeV
                  mass: float, # incident particle mass in MeV
                  I: float,    # mean excitation energy of the material
	          Z: int,
	          A: float,
	          z: int,
                  density: float,
                  **kwargs):
    gamma, beta = gammabeta(mass, mom)
    x     = dx*density
    c     = csi(x, beta, Z, A, z)
    me    = 0.5
    term1 = np.log(2*me*beta*beta*gamma*gamma/I*1000000)
    term2 = np.log(c/I*1000000)
    term3 = 0.2 - beta*beta
    term4 = delta(mom, mass, 'Helium')
    print(term1, term2, term3, term4)
    return c*(term1 + term2 + term3 - term4), c

def bethe_dE(dx: float, # thickness of detector in cm  
             mom: float,  # momentum in MeV
             mass: float, # incident particle mass in MeV
             I: float,    # mean excitation energy of the material
	     Z: int,
	     A: float,
	     z: int,
             density: float,
             **kwargs):
    K = 0.307075
    gamma, beta = gammabeta(mass, mom)
    const = K*z*z*Z/A/beta/beta
    me = 0.5
    Wmax  = 2*me*beta*beta*gamma*gamma/(1+2*me*gamma/mass + (me*me)/(mass*mass))
    term1 = 0.5*np.log(2*me*beta*beta*gamma*gamma*Wmax/I/I*1e12)
    term2 = beta*beta
    term3 = delta(mom, mass, 'Helium')/2
    return const*dx*density*(term1 - term2 - term3)


def trunc_mean(mlv: float,
               scale: float,
               cutoff: float = 0.6):
    x   = np.linspace(mlv-6*scale, mlv+20*scale, 10000)
    cdf = moyal.cdf(x, loc=mlv, scale=scale)
    idx = np.abs(cdf - 0.6*np.max(cdf)).argmin()
    trunc_mean = np.sum(x[idx]*moyal.pdf(x[:idx], loc=mlv, scale=scale)*(x[1]-x[0]))
    return trunc_mean

def plot_dEdx(particle1: dict,
              particle2: dict,
              material:  dict,
              **kwargs):
    momentum = np.logspace(2, 6, 1000)
    dEdx=[]
    for i in range(len(momentum)):        
        mlv1, scale1 = landau_params(dx=200,
                                     mom=momentum[i],
                                     **particle1,
                                     **helium)
        mlv2, scale2 = landau_params(dx=200,
                                     mom=momentum[i],
                                     **particle2,
                                     **helium)
        mlv1 = bethe_dE(dx=200,
                        mom=momentum[i],
                        **particle1,
                        **helium)
        mlv2 = bethe_dE(dx=200,
                        mom=momentum[i],
                        **particle2,
                        **helium)
        dEdx.append(np.abs(mlv1-mlv2)/((mlv1+mlv2)*0.043/2))


    ax.plot(momentum/1000, dEdx, label=f'dE/dx {particle1["name"]}-{particle2["name"]}', **kwargs)

def plot_dNdx(particle1: dict,
              particle2: dict,
              material:  dict,
              **kwargs):
    momentum = np.logspace(2, 6, 1000)
    dNdx=[]
    for i in range(len(momentum)):
        mlv1, scale1 = landau_params(dx=200,
                                     mom=momentum[i],
                                     **particle1,
                                     **material)
        print(mlv1)
        mlv2, scale2 = landau_params(dx=200,
                                     mom=momentum[i],
                                     **particle2,
                                     **material)
        mlv1 = bethe_dE(dx=200,
                        mom=momentum[i],
                        **particle1,
                        **helium)
        mlv2 = bethe_dE(dx=200,
                        mom=momentum[i],
                        **particle2,
                        **helium)
        print(mlv1)
        N2 = mlv2/material['I']*1e6
        N1 = mlv1/material['I']*1e6
        dNdx.append(np.abs(N1-N2)/np.sqrt((N1+N2)/2))
        
    ax.plot(momentum/1000, dNdx, label=f'dN/dx {particle1["name"]}-{particle2["name"]}', **kwargs)

helium = {'I': 41.8,
          'Z': 2,
          'A': 4.002602,
          'density': 0.0001663}

silicon = {'I': 173,
           'Z': 14,
           'A': 28.0855,
           'density': 2.329}

muon = {'z': 1,
        'mass': 105.7,
        'name': 'mu'}
pion = {'z': 1,
        'mass': 139.57,
        'name': 'pi'}
kaon = {'z': 1,
        'mass': 493.677,
        'name': 'K'}
proton = {'z': 1,
          'mass': 938.272,
          'name': 'p'}
strawTubes = {'N': 100,
              'r': 0.5}


fig, ax = plt.subplots()
ax.set_xlim([0.1,1000])
ax.set_ylim([0,12])
ax.yaxis.set_minor_locator(MultipleLocator(0.4))
ax.tick_params(axis='both', which='minor', direction='in', length=5, top=True, right=True)
ax.tick_params(axis='both', which='major', direction='in', length=10, top=True, right=True)
ax.set_xlabel(r'$p$ [GeV]', loc='right')
ax.set_ylabel(r'N $\sigma$', loc='top')
ax.set_xscale('log')

plot_dEdx(kaon, pion,   helium, color='blue', linestyle='--')
plot_dEdx(muon, pion,   helium, color='red', linestyle='--')
plot_dEdx(kaon, proton, helium, color='green', linestyle='--')
plot_dNdx(kaon, pion,   helium, color='blue', linestyle='-')
plot_dNdx(muon, pion,   helium, color='red', linestyle='-')
plot_dNdx(kaon, proton, helium, color='green', linestyle='-')
ax.legend(frameon=False, shadow=False, borderaxespad=1.0)
plt.show()
