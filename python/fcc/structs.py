import numpy as np

'''
Data structure to store individual hit
Has 4 data members:
 collection: the collection name from ddsim.
             disambiguates detector technologies
 x, y, z:    the 3D hit position
'''
class FCCHit:
    def __init__(self,
                 collection: str,
                 x: float,
                 y: float,
                 z: float):
        self.collection = collection
        self.x_pos      = x
        self.y_pos      = y
        self.z_pos      = z


'''
Data structure to hold an event
Has 2 members:

Implements convenience functions to count N hits
in different collections
'''
class FCCEvent:
    def __init__(self,
                 hits: list,
                 truth_pt: float):
        self.truth_pt = truth_pt
        self.hits = hits

    def xpos(self):
        return np.array([hit.x_pos for hit in self.hits])

    def ypos(self):
        return np.array([hit.y_pos for hit in self.hits])
    
    def NHits(self, collection_substr):
        return np.sum([collection_substr in hit.collection for hit in self.hits])
            
    def NVTXHits(self):
        return self.NHits('VTX')

    def NStrawHits(self):
        return self.NHits('MyReadout')

    def NDriftChamberHits(self):
        return self.NHits('CDCHHits')

    def NWrapperHits(self):
        return self.NHits('MyWrapper')
