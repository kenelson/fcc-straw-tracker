import numpy as np
from numpy.typing import NDArray
from abc import ABC, abstractmethod
from fcc.geometry import point_line_distance, bending_radius, bending_radius_to_pT
from scipy.optimize import minimize

hits_dtype = [('r', 'float64'),
              ('phi', 'float64'),
              ('res', 'float64'),
              ('x', 'float64'),
              ('y', 'float64')]

'''
Base class for parameterization of tracks.
Subclass is required to implement the chi2 method
'''
class TrackParameterization(ABC):

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

        if not hasattr(self, 'magnetic_field'):
            raise AttributeError(f'Must define magnetic_field attribute for {self.type}.  Pass as kwargs.')
        if not hasattr(self, 'cutoff'):
            self.cutoff = 999999 # this is number of sigma away from a track for a hit to be excluded

    '''
    Compute initial guess for minimization
    Implementation depends on the parameterization
    '''
    @abstractmethod
    def guess(self,
              hits: NDArray[hits_dtype]) -> list:
        pass

    '''
    Compute the jacobian vector
    Implementation depends on the parameterization
    '''
    @abstractmethod
    def jacobian(self,
                 args: list,
                 hits: NDArray[hits_dtype]) -> list:
        pass

    '''
    Convert this basis to the r, Xc, Yc basis
    Implementation depends on the parameterization
    '''
    @abstractmethod
    def rXcYc(self,
              args: list) -> tuple:
        pass

    '''
    Convert the basis to r, Xc, Yc and compute chi^2
    '''
    def chi2(self,
             args: list,
             hits: NDArray[hits_dtype]) -> np.float64:
        # compute alternative parameters
        rho, center_x, center_y = self.rXcYc(args)
        
        # compute chi^2
        chi2 = 0.0
        residual = self.residuals(rho, center_x, center_y, hits)
        hit_chi2 = np.power(residual,2)/hits['res']/hits['res']*1e12
        return np.sum(np.where(np.less(hit_chi2, self.cutoff*self.cutoff), hit_chi2, 0))

    
    '''
    Under the r, Xc, Yc basis compute the distance to each hit
    '''
    def distance(self,
                 rho: float,
                 center_x: float,
                 center_y: float,
                 hits: NDArray[hits_dtype]) -> NDArray:
        dx2 = hits['x'] - center_x
        dx2 = dx2*dx2
        dy2 = hits['y'] - center_y
        dy2 = dy2*dy2
        return np.sqrt(dx2+dy2)

    '''
    Once we convert to the r, Xc, Yc basis the residuals can be evaluated 
    under any parameterization
    '''
    def residuals(self,
                  rho: float,
                  center_x: float,
                  center_y: float,
                  hits: NDArray[hits_dtype]) -> NDArray:
        return self.distance(rho, center_x, center_y, hits) - rho

    
'''
Implementation of track parameterization in (pT, d0, psi) basis.
pT is used to calculate radius of curvature
d0 is the (signed) minimum distance from the origin.
psi is the angle to the center of the circle formed by the track.
'''
class PtPsiD0TrackParameterization(TrackParameterization):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
    
    def guess(self,
              hits: NDArray[hits_dtype]) -> list:
        psi_0_guess = np.fmod(np.mean(hits['phi'])+np.pi/2, np.pi*2)
        sagitta     = np.max(point_line_distance(hits['x'], hits['y'], hits['x'][0], hits['y'][0], hits['x'][-1], hits['y'][-1]))
        chord       = np.sqrt((hits['x'][0]-hits['x'][-1])*(hits['x'][0]-hits['x'][-1])+(hits['y'][0]-hits['y'][-1])*(hits['y'][0]-hits['y'][-1]))
        rho         = (chord*chord/8/sagitta + sagitta/2)
        pT_guess    = bending_radius_to_pT(rho, self.magnetic_field)
        return [pT_guess,psi_0_guess,0]

    def rXcYc(self,
              args: list) -> tuple:
        rho = bending_radius(args[0], self.magnetic_field)
        return (rho,
                (args[2] + rho)*np.cos(args[1]),
                (args[2] + rho)*np.sin(args[1]))
    
    def jacobian(self,
                 args: list,
                 hits: NDArray[hits_dtype]) -> list:
        pT_fit   = args[0]    # units are in GeV on pT
        psi      = args[1]
        d0       = args[2]

        rho, center_x, center_y = self.rXcYc(args)
        pT2Rho   = rho/pT_fit
        
        res      = hits['res']*1e-6
        x_dist   = hits['x'] - center_x
        y_dist   = hits['y'] - center_y

        # calculate the hit-by-hit chi^2 numerator
        # to help evaluate derivatives of chi2
        distance  = self.distance(rho, center_x, center_y, hits)
        residual  = distance-rho
        chi2_num  = np.power(residual, 2)

        # set flags to see which hits increase the chi^2 based on position relative to the circle
        is_top_half = np.less(np.fmod(hits['phi']-psi+np.pi*12, np.pi*2), np.pi)
        is_gr_rho = np.greater(hits['r'], (d0+rho)/np.cos(hits['phi']-psi))
        flags_pT  = np.less(residual, 0)
        flags_d0  = np.where(flags_pT, is_gr_rho, np.logical_not(is_gr_rho))
        flags_psi = np.where(flags_pT, is_top_half, np.logical_not(is_top_half))

        # calculate the derivative of chi^2 wrt pT
        jac_pT = 2*((-1.0*np.cos(psi)*x_dist - np.sin(psi)*y_dist)/distance - 1)*residual*pT2Rho
        jac_pT = np.abs(jac_pT/res/res)
        jac_pT = np.sum(np.where(flags_pT, jac_pT, -1.0*jac_pT))
        
        # calculate the derivative of chi^2 wrt d0
        jac_d0 = 2*residual/distance*(hits['x']*np.cos(psi)+hits['y']*np.sin(psi)-d0-rho)
        jac_d0 = np.abs(jac_d0/res/res)
        jac_d0 = np.sum(np.where(flags_d0, jac_d0, -1.0*jac_d0))    
        
        # calculate the derivative of chi^2 wrt psi
        jac_psi = 2*residual/distance*(rho+d0)*(hits['y']*np.cos(psi)-hits['x']*np.sin(psi))
        jac_psi = np.abs(jac_psi/res/res)
        jac_psi = np.sum(np.where(flags_psi, jac_psi, -1.0*jac_psi))
        
        return [jac_pT, jac_psi, jac_d0]
    
'''
Implementation of track parameterization in (pT, Xc, Yc) basis.
pT is used to caclulate the radius of curvature
Xc, Yc are the position of the center of the track.
'''
class PtXYTrackParameterization(TrackParameterization):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def guess(self,
              hits: NDArray[hits_dtype]) -> list:
        psi_0_guess = np.fmod(np.mean(hits['phi'])+np.pi/2, np.pi*2)
        sagitta     = np.max(point_line_distance(hits['x'], hits['y'], hits['x'][0], hits['y'][0], hits['x'][-1], hits['y'][-1]))
        chord       = np.sqrt((hits['x'][0]-hits['x'][-1])*(hits['x'][0]-hits['x'][-1])+(hits['y'][0]-hits['y'][-1])*(hits['y'][0]-hits['y'][-1]))
        rho         = (chord*chord/8/sagitta + sagitta/2)
        pT_guess    = bending_radius_to_pT(rho, self.magnetic_field)
        return [pT_guess,rho*np.cos(psi_0_guess),rho*np.sin(psi_0_guess)]

    def rXcYc(self,
              args: list) -> tuple:
        rho = bending_radius(args[0], self.magnetic_field)
        return (rho,
                args[1],
                args[2])
    
    def jacobian(self,
                 args: list,
                 hits: NDArray[hits_dtype]) -> list:
        rho, center_x, center_y = self.rXcYc(args)
                
        res      = hits['res']*1e-6
        x_dist   = hits['x'] - center_x
        y_dist   = hits['y'] - center_y

        # calculate the hit-by-hit chi^2 numerator
        # to help evaluate derivatives of chi2
        distance  = self.distance(rho, center_x, center_y, hits)
        residual  = distance-rho
        chi2_num  = np.power(residual, 2)

        # set flags to see which hits increase the chi^2 based on position relative to the circle
        is_above_yc = np.greater(hits['y'], center_y)
        is_right_xc = np.greater(hits['x'], center_x)
        flags_pT  = np.less(residual, 0)
        flags_xc  = np.where(flags_pT, is_right_xc, np.logical_not(is_right_xc))
        flags_yc  = np.where(flags_pT, is_above_yc, np.logical_not(is_above_yc))

        # calculate the derivative of chi^2 wrt pT 
        jac_pT = 2*residual
        jac_pT = np.abs(jac_pT/res/res)
        jac_pT = np.sum(np.where(flags_pT, jac_pT, -1.0*jac_pT))

        # calculate the derivative of chi^2 wrt xc
        jac_xc = 2*residual/distance*x_dist
        jac_xc = np.abs(jac_xc/res/res)
        jac_xc = np.sum(np.where(flags_xc, jac_xc, -1.0*jac_xc))
        
        # calculate the derivative of chi^2 wrt yc
        jac_yc = 2*residual/distance*y_dist
        jac_yc = np.abs(jac_yc/res/res)
        jac_yc = np.sum(np.where(flags_yc, jac_yc, -1.0*jac_yc))
        
        return [jac_pT, jac_yc, jac_xc]

'''
Simple data structure class for the LinearLeastSq method
'''
class MinimizeResult:
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

'''
Function to minimize the chi2 by the linearized least squares
method of Trilling
'''
def LinearLeastSqMinimize(chi2: callable,
                          guess: list,                          
                          **kwargs) -> MinimizeResult:
    hits             = kwargs['args'][0]
    parameterization = kwargs['parameterization']
    
    jac  = parameterization.jacobian
    
    parameters = guess
    update     = np.array([1.0 for _ in guess])
    tolerance  = 3e-6
    Niter      = 0
    
    while np.max(update) > tolerance and Niter < 300:
        # calculate derivative, residual for individual hits
        derivative = np.array([np.array(jac(parameters, h)) for h in hits])
        rho, center_x, center_y = parameterization.rXcYc(parameters)
        residuals  = parameterization.residuals(rho, center_x, center_y, hits)

        # increment matrices
        G = np.zeros((len(guess), len(guess)))
        Y = np.zeros(len(guess))
        for i in range(len(hits)):
            for r in range(len(guess)):
                for c in range(len(guess)):
                    G[r,c] += derivative[i,r]*derivative[i, c]#/hits['err'][i]/hits['err'][i]
                Y[r] += residuals[i]*derivative[i,r]#/hits['err'][i]/hits['err'][i]

        try:
            delta_params = -1.0 * np.dot(np.linalg.inv(G), Y)
        except:
            return MinimizeResult(x=parameters,success=False)
        update       = [np.abs(d) for d in delta_params]
        parameters   = [p + d for p, d in zip(parameters, list(delta_params))]
        Niter += 1
    print(f'Converged in {Niter} iterations')
    return MinimizeResult(x=parameters, success=True)

'''
Base class for optimization interface
Subclass must write initialization
which can optionally 
'''
class Optimizer(ABC):

    @abstractmethod
    def __init__(self, parameterization):
        self.parameterization = parameterization

    def check_setup(self):
        if not hasattr(self, 'method'):
            raise AttributeError('Optimizer {self.type} has no optimization method set')

        if not hasattr(self, 'parameterization'):
            raise AttributeError('Optimizer {self.type} has no parameterization method set')

        if not hasattr(self, 'default_kwargs'):
            raise AttributeError('Optimizer {self.type} has no default_kwargs set')
        
    def optimize(self,
                 hits: NDArray[hits_dtype],
                 **kwargs):        
        return self.method(self.parameterization.chi2,
                           self.parameterization.guess(hits),
                           args=(hits,),
                           **kwargs, **self.default_kwargs)

'''
Use the simplex method.  No jacobian required.
'''
class SimplexOptimizer(Optimizer):

    def __init__(self, parameterization):
        super().__init__(parameterization)

        self.method = minimize
        self.default_kwargs = {'method': 'Nelder-Mead',
                               'tol': 1e-8,
                               'options': {
                                   'disp': True,
                                   'maxiter': 799
                               }}
        
        self.check_setup()

'''
Use the NewtonCG method.  Requires jacobian.
'''
class NewtonCGOptimizer(Optimizer):

    def __init__(self, parameterization):
        super().__init__(parameterization)

        self.method = minimize
        self.default_kwargs = {'method': 'Newton-CG',
                               'jac': self.parameterization.jacobian,
                               'options': {
                                   'disp': True,
                                   'maxiter': 799
                                   }}
        self.check_setup()

'''
Use linearized least square residuals and matrix inversion
to calculate the step
'''
class LinearLeastSqOptimizer(Optimizer):

    def __init__(self, parameterization):
        super().__init__(parameterization)

        self.method = LinearLeastSqMinimize
        self.default_kwargs = {'parameterization': self.parameterization}
        
        self.check_setup()
