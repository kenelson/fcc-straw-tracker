import numpy as np
import matplotlib.pyplot as plt
import os

def extract_data_from_file(file):
    with open(file, 'r') as f:
        lines = f.readlines()
    sigma, sigma_error = None, None
    for line in lines:
        if "sigma:" in line:
            sigma = float(line.split(":")[1].strip())
        elif "sigma_error:" in line:
            sigma_error = float(line.split(":")[1].strip())
    return sigma, sigma_error if sigma is not None and sigma_error is not None else (None, None)

# Define materials and configurations
materials = ['Ar', 'He']
configurations = {
    f'{material}_vs100': (material, 'vs', 100) for material in materials
}
configurations.update({
    f'{material}_vs120': (material, 'vs', 120) for material in materials
})
configurations.update({
    f'{material}_vsw100': (material, 'vsw', 100) for material in materials
})
configurations.update({
    f'{material}_vsw120': (material, 'vsw', 120) for material in materials
})
print(configurations)
pT_values = list(range(10, 101, 5))
results = {key: {'values': [], 'errors': [], 'pT_values': []} for key in configurations.keys()}

for config_key, (material, config, resolution) in configurations.items():
    for pT in pT_values:
        file_name = f'Gaussian_Chihao_{material}_{config}_{pT}_{resolution}.txt'
        file_path = os.path.join(file_name)
        if os.path.isfile(file_path):
            sigma, sigma_error = extract_data_from_file(file_path)
            if sigma is not None and sigma_error is not None:
                results[config_key]['values'].append(np.abs(sigma))
                results[config_key]['errors'].append(np.abs(sigma_error) if np.abs(sigma_error) < 1 else 0)
                results[config_key]['pT_values'].append(pT)
print(results)
# Plot 1: Only Ar data
plt.figure(figsize=(10, 7))
for config_key in results.keys():
    if 'Ar' in config_key:
        plt.errorbar(results[config_key]['pT_values'], results[config_key]['values'], yerr=results[config_key]['errors'], fmt='o', label=config_key)
plt.xlabel('pT value: GeV')
plt.ylabel('Resolution')
plt.title('Resolution vs pT Value for Ar Configurations')
plt.legend()
plt.tight_layout()
plt.savefig('Resolution_Ar.png')
plt.clf()

# Plot 2: Only He data
plt.figure(figsize=(10, 7))
for config_key in results.keys():
    if 'He' in config_key:
        plt.errorbar(results[config_key]['pT_values'], results[config_key]['values'], yerr=results[config_key]['errors'], fmt='o', label=config_key)
plt.xlabel('pT value: GeV')
plt.ylabel('Resolution')
plt.title('Resolution vs pT Value for He Configurations')
plt.legend()
plt.tight_layout()
plt.savefig('Resolution_He.png')
plt.clf()

# Plot 3: Only 100 resolution
plt.figure(figsize=(10, 7))
for config_key in results.keys():
    if '100' in config_key:
        plt.errorbar(results[config_key]['pT_values'], results[config_key]['values'], yerr=results[config_key]['errors'], fmt='o', label=config_key)
plt.xlabel('pT value: GeV')
plt.ylabel('Resolution')
plt.title('Resolution vs pT Value for 100 Resolution')
plt.legend()
plt.tight_layout()
plt.savefig('Resolution_100.png')
plt.clf()

# Plot 4: Only 120 resolution
plt.figure(figsize=(10, 7))
for config_key in results.keys():
    if '120' in config_key:
        plt.errorbar(results[config_key]['pT_values'], results[config_key]['values'], yerr=results[config_key]['errors'], fmt='o', label=config_key)
plt.xlabel('pT value: GeV')
plt.ylabel('Resolution')
plt.title('Resolution vs pT Value for 120 Resolution')
plt.legend()
plt.tight_layout()
plt.savefig('Resolution_120.png')
plt.clf()

# Plot 5: All data together
plt.figure(figsize=(10, 7))
for config_key in results.keys():
    plt.errorbar(results[config_key]['pT_values'], results[config_key]['values'], yerr=results[config_key]['errors'], fmt='o', label=config_key)
plt.xlabel('pT value: GeV')
plt.ylabel('Resolution')
plt.title('Resolution vs pT Value for All Configurations')
plt.legend()
plt.tight_layout()
plt.savefig('Resolution_All.png')
plt.clf()
