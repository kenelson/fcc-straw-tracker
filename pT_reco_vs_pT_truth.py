"""
python gfit.py [material] [vs or vsw] [energy] [resolution_straw]
it will do gaussian fit for that and generate files like Gaussian_Chihao_Ar_vs_10_100.txt that stores pT, mu, sigma, mu_error, sigma_error
Also it will generate gaussian fit plot like Gaussian_Chihao_Ar_vs_10_100.png
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from scipy.optimize import minimize, curve_fit, root_scalar
from scipy.optimize import NonlinearConstraint
import scipy
from scipy.stats import gamma, chi2, norm
import pickle
import os  ## This module is for "operating system" interfaces
import sys

path_workdir = '/atlas/data18a/joarias/fcc-straw-tracker/'
path_fits_figs = path_workdir + 'gfits_imgs_txt_files/'
root_files_path = path_workdir + 'root_files/'
npy_files_path = path_workdir + 'npy_files/'
pT_fit_txt_files_path = path_workdir + 'pT_fit_txt_files/'

plt.style.use('mplstyle.txt')
plt.rcParams["mathtext.fontset"] = "cm"
    
## read sys
if len(sys.argv) != 5:
    print("Usage: python simulation_v03.py <integer>")
    sys.exit(1)

material = sys.argv[1]
config = sys.argv[2]
energy = int(sys.argv[3])
resolution_straw = int(sys.argv[4])
print(f"Received input: {energy}")

setup=f"Chihao_{material}_{config}_{energy}_{resolution_straw}"

## cutoff for gaussian fit
def cutoff(pT):
    return 0.01*np.log10(pT) + 0.01

## Gaussian function for fit
def gaussian_model(x, mu, sigma, amplitude):
    return amplitude * (1 / np.sqrt(2 * np.pi*(sigma)**2)) * np.exp(-(x - mu) ** 2 / (2 * sigma ** 2))

## Reading pT_resolution from file and converting to float; outputs of run_tracking.py
## pT_fit_Chihao_Ar_vs_10_100.txt
with open(pT_fit_txt_files_path + f'pT_fit_{setup}.txt', 'r') as file:
    # pT_resolution = [float(line.strip()) for line in file]
    pT_resolution = [float(line.strip()) for line in file]

pT_truth = np.ones(len(pT_resolution))*energy
pT_reco = pT_truth/(1.-np.array(pT_resolution))
# print(np.shape(pT_reco), np.shape(pT_truth))
# print(pT_reco[:5])

# Combine into a two-column array
response_mtx = np.column_stack((pT_truth, pT_reco))

# Save to a text file
output_path = path_fits_figs + f'pT_reco_truth_{setup}_{energy}_{resolution_straw}.txt'
np.savetxt(output_path, response_mtx, fmt="%.6f", header="pT_truth pT_reco", comments='')
